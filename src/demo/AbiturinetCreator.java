package demo;

import model.Abiturient;
import java.util.concurrent.ThreadLocalRandom;

public class AbiturinetCreator {

    public Abiturient[] createAbiturients(int number) {

        Abiturient[] abiturients = new Abiturient[number];

        for (int i = 0; i < number; i++) {
            abiturients[i] = createAbiturient();
        }

        return abiturients;
    }

    private Abiturient createAbiturient() {

        return new Abiturient(createString(getRandom(3, 11)), createString(getRandom(3, 11)), generateMarks(6));

    }

    private int[] generateMarks(int length) {

        int loser = 20;
        int random = getRandom(0, 100);

        if (random > loser) {
            return makeAverage(length);
        } else {
            return makeLoser(length);
        }

    }

    private int[] makeAverage(int length) {

        int[] marks = new int[length];

        for (int i = 0; i < length; i++) {
            int random = getRandom(3, 6);
            marks[i] = random;
        }
        return marks;
    }

    private int[] makeLoser(int length) {

        int[] marks = new int[length];

        for (int i = 0; i < length; i++) {
            int random = getRandom(1, 5);
            marks[i] = random;
        }
        return marks;
    }

    private String createString(int length) {

        String symbols = "abcdefhijkprstuvwx";
        StringBuilder SB = new StringBuilder(length);

        SB.append(symbols.toUpperCase().charAt(getRandom(0, symbols.length() - 1)));

        for (int i = 1; i < length; i++) {
            SB.append(symbols.charAt(getRandom(0, symbols.length() - 1)));
        }

        return SB.toString();
    }

    private int getRandom (int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }

}
