package demo;

import model.Abiturient;
import service.IAbiturientService;

public class DemoService {

    private IAbiturientService service;
    private Abiturient[] abiturients;

    public DemoService(IAbiturientService service, Abiturient[] abiturients) {
        this.service = service;
        this.abiturients = abiturients;
    }

    public void execute() {

        System.out.printf("\n%50s\n\n", "Все абитуриенты:");
        System.out.printf("%-2s %-11s %-17s %-15s %-15s %s\n",
                "№", "Имя:", "Фамилия:", "Оценки:", "Средний балл:", "Может поступить:");

        int i = 1;

        for (Abiturient abiturient : abiturients) {
            service.avgAbiturientMark(abiturient);
            System.out.printf("%-3d %-50s %-15.2f %s\n",
                    i++, abiturient.toString(), abiturient.getAvgMark(), service.isAbiturientCanEnter(abiturient));
        }

        int placeNumber = 5;

        System.out.println("----------------------------------------------------------------------------------");

        System.out.printf("%40s (мест %d):\n\n", "Поступившие абитуриенты", placeNumber);
        System.out.printf("%-2s %-11s %-17s %-15s %s\n",
                "№", "Имя:", "Фамилия:", "Оценки:", "Средний балл:");

        i = 1;

        for (Abiturient abiturient : service.findEnteredAbiturients(abiturients, placeNumber)){
            System.out.printf("%-3d %-50s %.2f\n",
                    i++, abiturient.toString(), abiturient.getAvgMark());
        }

    }
}
