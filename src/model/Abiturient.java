package model;

import java.util.Arrays;

public class Abiturient {

    private String name;
    private String surname;
    private int[] marks;
    private float avgMark;

    public Abiturient(String name, String surname, int[] marks) {
        this.name = name;
        this.surname = surname;
        this.marks = marks;
    }

    public Abiturient(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public int[] getMarks() {
        return marks;
    }

    public void setMarks(int[] marks) {
        this.marks = marks;
    }

    public float getAvgMark() {
        return avgMark;
    }

    public void setAvgMark(float avgMark) {
        this.avgMark = avgMark;
    }

    @Override
    public String toString() {
        return  String.format("%-11s %-11s %s", name, surname, Arrays.toString(marks));
    }
}
