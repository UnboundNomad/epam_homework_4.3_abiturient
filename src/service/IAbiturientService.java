package service;

import model.Abiturient;

public interface IAbiturientService {

    boolean isAbiturientCanEnter(Abiturient abiturient);

    void avgAbiturientMark (Abiturient abiturient);

    Abiturient[] findEnteredAbiturients (Abiturient[] abiturients, int placeNumber);

}
