package service;

import model.Abiturient;

import java.util.Arrays;
import java.util.stream.Collectors;

public class AbiturientService implements IAbiturientService {

    @Override
    public void avgAbiturientMark(Abiturient abiturient) {

        float result = 0;

        for (int mark : abiturient.getMarks()) {
            result += mark;
        }

        abiturient.setAvgMark(result / abiturient.getMarks().length);

    }

    @Override
    public Abiturient[] findEnteredAbiturients(Abiturient[] abiturients, int placeNumber) {

        return Arrays.stream(abiturients)
                .filter(this::isAbiturientCanEnter)
                .sorted((o1, o2) -> Float.compare(o2.getAvgMark(), o1.getAvgMark()))
                .limit(placeNumber)
                .collect(Collectors.toList())
                .toArray(new Abiturient[placeNumber]);

    }

    @Override
    public boolean isAbiturientCanEnter(Abiturient abiturient) {

        for (int mark : abiturient.getMarks()) {
            if (mark < 3) {
                return false;
            }
        }
        return true;
    }
}
